<?php namespace eBossApi;

/** @var \Herbert\Framework\Enqueue $enqueue */


$enqueue->admin([
    'as'  => 'adminbootstrapCSS',
    'src' => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css',
    'filter' => [ 'panel' => [ 'mainPanel' ] ]
]);

$enqueue->front([
    'as'  => 'bootstrapCSS',
    'src' => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css'
]);

$enqueue->front([
    'as'  => 'bootstrapJS',
    'src' => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js'
], 'footer');

$enqueue->front([
	'as' => 'jQueryJS',
	'src'=> '//code.jquery.com/jquery-1.10.2.js'
],'footer');

$enqueue->front([
    'as'  => 'ajax-script',
    'src' => Helper::assetUrl('/js/main.js')
], 'footer');

$enqueue->admin([
    'as' => 'angular-js',
    'src' => 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js'
]);

$enqueue->admin([
    'as' => 'angular-app',
    'src' => Helper::assetUrl('/js/admin/app.js'),
]);

$enqueue->admin([
    'as' => 'angular-controller',
    'src' => Helper::assetUrl('/js/admin/controller.js'),
]);