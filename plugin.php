<?php

/**
 * @wordpress-plugin
 * Plugin Name:       eBoss Recruitment API
 * Plugin URI:        http://ebossapiplugin.net/
 * Description:       eBoss+ Recuriment API Software http://www.ebossrecruitment.com/
 * Version:           1.0.0
 * Author:            Vinay Shah
 * Author URI:        http://vinayshah.in/
 * License:           MIT
 */

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/vendor/getherbert/framework/bootstrap/autoload.php';
