
jQuery(function( $ ){
// ajaxRegionAction
	$('#country').change(function(){

		// var data1 = {
		// 	action : 'ajaxRegionAction',
		// 	country_id : this.value
		// };

		$.ajax({
			url		: ajax_object.ajaxurl,
			data 	: {country_id : this.value},
			dataType: 'json',
			method 	: 'GET',
			type 	: 'post',
			success : function(result,status,xhr){
				console.log(xhr);
			},
		});
	});

	// accepts number only for radius textfield
	$("#mile-radius").keydown(function(event) {
		// Allow only backspace and delete
		if ( event.keyCode == 46 || event.keyCode == 8 ||  event.keyCode == 190 ) {
			// let it happen, don't do anything
		}
		else {
			// Ensure that it is a number and stop the keypress
			if (event.keyCode < 48 || event.keyCode > 57 ) {
				event.preventDefault();
			}
		}
	});

	//show loader
	$('.btn').click(function(){
		$('#loader').fadeIn();
	});


	//toggle apply with cv only

	$('#cv-toggle-option').click(function(){
		var $this = $(this);

		if ($this.is(':checked')) {
			$('#cv-only').css('display','block');
			$('#cv-and-form').css('display','none');
		} else {
			$('#cv-only').css('display','none');
			$('#cv-and-form').css('display','block');
		}
	});
}(jQuery));